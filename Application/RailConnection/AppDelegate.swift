//
//  AppDelegate.swift
//  RailConnection
//
//  Created by August Saint Freytag on 2021-05-03.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

	var statusBarItem: StatusBarItem!

	func applicationDidFinishLaunching(_ notification: Notification) {
		// Insert code here to initialize your application
		statusBarItem = StatusBarItem()
		statusBarItem.setUpItem()
	}

	func applicationWillTerminate(_ notification: Notification) {
		// Insert code here to tear down your application
	}

}
