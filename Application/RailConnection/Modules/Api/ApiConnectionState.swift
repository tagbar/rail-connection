//
//  ConnectionState.swift
//  RailConnection
//
//  Created by August Saint Freytag on 25/05/2021.
//

import Foundation

// NEXT UP: Actually fetch and decode the Bahn API response into this struct as a value.

struct ApiConnectionState: Codable {

	let connection: Bool

	let serviceLevel: String

	let gpsStatus: String

	let internet: String

	let latitude: Double

	let longitude: Double

	let tileY: Int

	let tileX: Int

	let series: String

	let serverTime: Date

	let speed: Int

	let trainType: String

	let tzn: String

	let wagonClass: String

	let connectivity: ApiConnectivity

}

struct ApiConnectivity: Codable {

	let currentState: String

	let nextState: String

	let remainingTimeSeconds: Int

}
