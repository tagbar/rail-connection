//
//  ApiConnectionStateDecoder.swift
//  RailConnection
//
//  Created by August Saint Freytag on 02/06/2021.
//

import Foundation

protocol ApiConnectionStateDecoder {}

extension ApiConnectionStateDecoder {

	func decodeData(from data: Data) -> ApiConnectionState? {
		let decoder = JSONDecoder()
		decoder.dateDecodingStrategy = .millisecondsSince1970

		do {
			return try decoder.decode(ApiConnectionState.self, from: data)
		} catch {
			assertionFailure(error.localizedDescription)
			return nil
		}
	}
}

