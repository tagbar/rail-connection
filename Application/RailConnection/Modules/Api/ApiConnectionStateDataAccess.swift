//
//  ConnectionStateDataAccess.swift
//  RailConnection
//
//  Created by August Saint Freytag on 2021-05-04.
//

import Foundation

protocol ApiConnectionStateDataAccess {

	typealias CompletionHandler<DataType> = (_ data: DataType?, _ error: Error?) -> Void

}

extension ApiConnectionStateDataAccess {

	var url: URL { URL(string: "http://localhost:8080/")! }

	func fetchData(_ completionHandler: @escaping CompletionHandler<Data>) {
		let requestUrl = url
		let request = URLRequest(url: requestUrl)

		let requestTask = URLSession.shared.dataTask(with: request) { data, response, error in
			completionHandler(data, error)
		}

		requestTask.resume()
	}

	func fetchJSONResponse(_ completionHandler: @escaping CompletionHandler<String>) {
		fetchData { data, error in
			guard
				let data = data,
				let encodedJSONString = String(data: data, encoding: .utf8)
			else {
				completionHandler(nil, error)
				return
			}

			completionHandler(encodedJSONString, error)
		}
	}
}
