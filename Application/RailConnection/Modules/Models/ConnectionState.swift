//
//  ConnectionState.swift
//  RailConnection
//
//  Created by August Saint Freytag on 02/06/2021.
//

import Foundation

struct ConnectionState {

	let current: ConnectionQuality
	let predicted: ConnectionQuality
	let remainingTime: TimeInterval

}

extension ConnectionState {

	init?(from apiConnectionState: ApiConnectionState) {
		let connectivity = apiConnectionState.connectivity

		guard
			let current = ConnectionQuality(rawValue: connectivity.currentState),
			let predicted = ConnectionQuality(rawValue: connectivity.nextState)
		else {
			return nil
		}

		self.current = current
		self.predicted = predicted
		self.remainingTime = TimeInterval(connectivity.remainingTimeSeconds)
	}

}

extension ConnectionState {

	enum ConnectionQuality: String {
		case high = "HIGH"
		case weak = "WEAK"
		case unstable = "UNSTABLE"
	}

}
