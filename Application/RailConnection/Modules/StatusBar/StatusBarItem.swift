//
//  StatusBarIcon.swift
//  RailConnection
//
//  Created by August Saint Freytag on 2021-05-04.
//

import Cocoa

class StatusBarItem {

	var statusBarIconName: NSImage.Name { "Menu Bar Image" }

	var statusBarIconSize: NSSize { .init(width: 16, height: 16) }

	let statusBarItem: NSStatusItem

	init() {
		statusBarItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.squareLength)
	}

	func setUpItem() {
		guard let button = statusBarItem.button else {
			assertionFailure("Expected created status bar item to have button, none returned.")
			return
		}

		setUpButton(button)

		fetchConnectionState { connectionState in
			self.reloadItemData(with: connectionState)
		}
	}

	func reloadItemData(with connectionState: ConnectionState) -> Void {
		statusBarItem.menu = menu(for: connectionState)
	}

	var icon: NSImage? {
		guard let icon = NSImage(named: statusBarIconName) else {
			return nil
		}

		icon.size = statusBarIconSize
		return icon
	}

	func menu(for connectionState: ConnectionState) -> NSMenu {
		let menu = NSMenu(title: "Connectivity")

		let firstMenuItem = NSMenuItem()
		firstMenuItem.title = "\(connectionState.current)"
		menu.addItem(firstMenuItem)

		let secondMenuItem = NSMenuItem()
		secondMenuItem.title = "\(connectionState.predicted) in \(connectionState.remainingTime)s"
		menu.addItem(secondMenuItem)

		return menu
	}

	func setUpButton(_ button: NSStatusBarButton) {
		button.image = icon

		button.action = #selector(didClickMenuBarItem(_:))
		button.target = self
	}

	@objc func didClickMenuBarItem(_ sender: AnyObject) {
		print("Did click menu bar item.")
	}

}

extension StatusBarItem: ApiConnectionStateDataAccess, ApiConnectionStateDecoder {
	func fetchConnectionState(_ callback: @escaping (_ connectionState: ConnectionState) -> Void) -> Void {
		fetchData { data, error in
			guard
				let data = data,
				let apiConnectionState = self.decodeData(from: data),
				let connectionState = ConnectionState(from: apiConnectionState)
			else {
				return
			}

			callback(connectionState)
		}
	}
}
