import { serve } from "https://deno.land/std@0.95.0/http/server.ts"

const server = serve({ port: 8080 })
console.log(`HTTP webserver running.  Access it at:  http://localhost:8080/`)

const prefabResponse = {
	connection: true,
	serviceLevel: "AVAILABLE_SERVICE",
	gpsStatus: "VALID",
	internet: "HIGH",
	latitude: 51.40204633333333,
	longitude: 12.386520333333333,
	tileY: 155,
	tileX: 199,
	series: "803",
	serverTime: 1620027395581,
	speed: 139,
	trainType: "ICE",
	tzn: "Tz155",
	wagonClass: "FIRST",
	connectivity: { currentState: "HIGH", nextState: "WEAK", remainingTimeSeconds: 412 }
}

for await (const request of server) {
	const response = {
		status: 200,
		headers: new Headers(), 
		body: JSON.stringify(prefabResponse)
	}

	response.headers.set("Content-Type", "application/json")

	request.respond(response)
}